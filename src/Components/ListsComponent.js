import React from 'react';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

class ListsComponent extends React.Component{
    constructor(props){
        super(props)
        this.state={
            listsValue:[],
        }
    }
    componentDidMount(){
        console.log('listsValues ::',this.props.listsValue)
        this.setState({listsValue:this.props.listsValue})
    }
    static getDerivedStateFromProps(props, state){
        if(props.listsValue!=state.listsValue){
            return ({listsValue:props.listsValue});
        }
        return null;
    }
    handleChange = value => {
        console.log('handleChange in child ::', value, this.state.listsValue);
        let temp=this.state.listsValue;
        let selectedObject={};
        for(let i=0;i<temp.length;i++){
            if(temp[i].value==value.value){
                temp[i].checked=!temp[i].checked;
                selectedObject=temp[i];
            }
        }
        console.log(temp)
        this.setState({
            listsValue:temp    
        },()=>{
            console.log('selectedObject ', selectedObject);
            this.props.handleChange(selectedObject)
        })
    }
    render(){
        return(
            <Paper className="paper">
                <List className="roorClass">
                    {this.state.listsValue && this.state.listsValue.length>0 && this.state.listsValue.map(value => {
                    const labelId = `checkbox-list-label-${value.id}`;

                    return (
                        <ListItem key={value.value} role={undefined} dense button onClick={()=>this.handleChange(value)}>
                            <ListItemIcon>
                            <Checkbox
                            edge="start"
                            checked={value.checked}
                            tabIndex={-1}
                            disableRipple
                            inputProps={{ 'aria-labelledby': labelId }}
                            /></ListItemIcon>
                        <ListItemText id={labelId} primary={value.value} />
                        </ListItem>
                    );
                    })}
                </List>
            </Paper>
        )
    }
};
export default ListsComponent;