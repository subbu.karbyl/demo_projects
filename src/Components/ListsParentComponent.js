import React from 'react';
import ListsComponent from './ListsComponent';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

let pairObjectTemp={};
let temp={};
export default class ListsParentComponent extends React.Component{
    constructor(props){
        super(props)
        this.state={
            dummy1:[{id:1, value:'Item-A', checked:false, list:1},
                    { id:2, value:'Item-B', checked:false, list:1},
                    {id:3, value:'Item-C', checked:false, list:1},
                    {id:4, value:'Item-D', checked:false, list:1}],
            dummy2:[{ id:1, value:'Item-W', checked:false, list:2},
                    {  id:2, value:'Item-X', checked:false, list:2},
                    { id:3, value:'Item-Y', checked:false, list:2},
                    { id:4, value:'Item-Z', checked:false, list:2}],
            pairObject:{},
        }
    }
    handleLeft = value => () => {
      console.log(value)
      
    };
     handleRight = value =>{
      console.log(value)
    }
    handleChange= value =>{
        console.log(' handleChange called in paret ::',value);
        temp=this.state.pairObject;
        if(value.list==1){
            pairObjectTemp.key= value.checked ? value.value : '';
            //pairObjectTemp.list=value.list;
            pairObjectTemp.value='';
        }
        if(value.list==2){
            console.log('handleChange value :::::::: ', value, pairObjectTemp.value)
            if(pairObjectTemp.value==''){
                pairObjectTemp.value=value.checked ? value.value :'';
                temp[pairObjectTemp.key]=pairObjectTemp.value;

                console.log('pairObject ', temp)
                //this.setState({pairObject:temp}, console.log(this.state.pairObject))
            }
            else if(!!!pairObjectTemp.key || pairObjectTemp.key=='' || pairObjectTemp.key==null){
                console.log('key doesnot exist');
                alert('please select item from fitst list')
            }
            else if(!value.checked){
                pairObjectTemp.value='';
                temp[pairObjectTemp.key]=pairObjectTemp.value;
            }
            pairObjectTemp={}
        }
        console.log('pairObjectTemp key ', pairObjectTemp)
    }

    handleSubmit = () =>{
        for (var propName in temp) { 
            if (temp[propName] === null || temp[propName] === undefined || temp[propName]=='') {
                delete temp[propName];
            }
        }
        this.setState({pairObject:temp}, console.log(this.state.pairObject))
    }
    render(){
        return( 
            <Grid container spacing={3}>
                <Grid item xs={3}>
                </Grid>
                <Grid item xs={3}>
                    <ListsComponent listsValue={this.state.dummy1} handleChange={this.handleChange} />
                </Grid>
                <Grid item xs={3}>
                    <ListsComponent listsValue={this.state.dummy2} handleChange={this.handleChange} />
                </Grid>
                <Grid item xs={3}>
                </Grid>
                <Grid item xs={5}>
                </Grid>
                <Grid item xs={2}>
                    <Button variant="contained" color="primary" disableElevation onClick={this.handleSubmit}>
                        Submit
                    </Button>
                </Grid>
                <Grid item xs={5}>
                </Grid>
            </Grid>
        )
    }
};