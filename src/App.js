import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListsParentComponent from './Components/ListsParentComponent';

function App() {

  return (
    <div className="App">
      <ListsParentComponent />
    
    </div>
  );
}

export default App;
